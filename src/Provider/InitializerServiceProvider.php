<?php

namespace EP\LaravelInitializer\Provider;

use Illuminate\Support\ServiceProvider;

class InitializerServiceProvider extends ServiceProvider
{
    protected $commands = [
        'EP\LaravelInitializer\Commands\InstallSpecificPackage',
        'EP\LaravelInitializer\Commands\InstallDefaultPackages',
        'EP\LaravelInitializer\Commands\ConfigureSpecificPackage',
        'EP\LaravelInitializer\Commands\ConfigureDefaultPackages'

    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'initializer');
        $this->mergeConfigFrom(__DIR__.'/../config/extra-steps.php', 'extra-steps');
        $this->commands($this->commands);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('initializer.php'),
        ], 'config');
    }
}
