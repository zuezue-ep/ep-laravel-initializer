<?php

namespace EP\LaravelInitializer\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class ConfigureSpecificPackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initializer:configure-package';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure user-defined composer package';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name = Cache::get('specific');

        $predefinedNames = config('initializer');
        $extraSteps = config('extra-steps');

        if (!array_key_exists($name, $predefinedNames)) {
           $this->error("Please specify the correct package name");
           return;
        }
        $package = $predefinedNames[$name];
        

        if (!array_key_exists($name, $extraSteps)) {
            return;
        }

        $steps = $extraSteps[$name];
        foreach($steps as $step) {
            if (str_contains($step, 'vendor:publish')) {
                preg_match('^.*=(.*?)$^', $step, $matches);
                $this->call('vendor:publish', ['--provider'=>$matches[1]]);
            } else {
                $this->call($step);
            }
        }
        $this->info("Woo hoo! {$package} is ready to use...");

        Cache::forget('specific');
    }
}
