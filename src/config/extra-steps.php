<?php

return [
    "debugger" => [
        'telescope:install',
        'migrate'
    ],
    "role-permission" => [
        'vendor:publish --provider=Spatie\Permission\PermissionServiceProvider',
        'config:clear',
        'migrate'
    ],
    "authentication" => [
        'migrate',
        'passport:install'
    ],
    "queue" => [
        'horizon:install'
    ],
    "search" => [
        'vendor:publish --provider=Laravel\Scout\ScoutServiceProvider'
    ],
    "payment" => [
        'migrate',
        'vendor:publish --tag=cashier-migrations'
    ],
    "tenancy"  => [
        'tenancy:install'
    ],
    "coding-standard" => [
        'vendor:publish --provider=STS\Fixer\FixerServiceProvider'
    ],
    "modularizer"   => [
        'vendor:publish --tag=config'
    ],
    "api-documentation" => [
        'vendor:publish --provider=AndreasElia\PostmanGenerator\PostmanGeneratorServiceProvider --tag=postman-config'
    ]
];