<?php

return [
    "modularizer"           => "escapepixel/laravel-ca-modules",
    "tenancy"               => "stancl/tenancy",
    "slug"                  => "spatie/laravel-sluggable",
    "role-permission"       => "spatie/laravel-permission",
    "social-integration"    => "laravel/socialite",
    "debugger"              => "laravel/telescope",
    "authentication"        => "laravel/passport",  
    "cache"                 => "predis/predis",
    "search"                => "algolia/scout-extended:^2.0",
    "payment"               => "laravel/cashier",
    "image-processor"       => "intervention/image",
    "error-monitoring"      => "sentry/sentry-laravel",
    "coding-standard"       => "stechstudio/laravel-php-cs-fixer",
    "mail-testing"          => "spatie/laravel-mailable-test",
    "aws"                   => "aws/aws-sdk-php",
    "auditing"              => "owen-it/laravel-auditing",
    "avatar"                => "laravolt/avatar",
    "api-documentation"     => "andreaselia/laravel-api-to-postman"
];